import React from "react";
import logo from "./logo.svg";
import Form from "./components/Form";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" /> Saisie de notes
      </header>
      <main>
        <Form amount={2} />
      </main>
    </div>
  );
}

export default App;
