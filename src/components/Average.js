import React from "react";
import "./Average.scss";

const Average = (props) => {
  const { notes, reset } = props;
  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  const totalCoeff = notes
    .map((note) => {
      return note.coeff === "" ? 1 : parseFloat(note.coeff);
    })
    .reduce(reducer);

  const totalNotes = notes
    .map((note) => {
      let maxNote = note.note > 20 ? 20 : note.note;
      let coeff = note.coeff === "" ? 1 : parseFloat(note.coeff);
      return parseFloat(maxNote * coeff);
    })
    .reduce(reducer);

  const average = totalNotes / totalCoeff;

  console.log(totalCoeff, totalNotes, average);
  return (
    <>
      <h2>Moyenne :</h2>
      <strong>{average.toFixed(2)}</strong>
      <br />
      <br />
      <button onClick={(e) => reset(null, "RESET")}>Reset</button>
    </>
  );
};

export default Average;
