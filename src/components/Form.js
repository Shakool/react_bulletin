import React, { useReducer, useState, useEffect } from "react";
import "./Form.scss";
import { reducer } from "../reducers/notes";
import InputGroup from "./InputGroup";
import Average from "./Average";

const Form = (props) => {
  const { amount } = props;
  const initialState = { notes: [...Array(amount)] };
  const [state, dispatch] = useReducer(reducer, initialState);
  const [noteCount, setNoteCount] = useState(0);
  const [displayAverage, setDisplayAverage] = useState(false);

  useEffect(() => {
    setDisplayAverage(noteCount === amount);
    return () => {};
  }, [noteCount]);

  const actions = (payload, action) => {
    if ((action === "ADD" && payload.note !== "") || action !== "ADD") {
      action === "ADD" ? setNoteCount((noteCount) => noteCount + 1) : setNoteCount((noteCount) => noteCount - 1);
      dispatch({ type: action, payload: payload });
    }
  };

  const formGroups = state.notes.map((el, i) => {
    if (el === undefined) {
      return <InputGroup id={i} key={i} changed={actions} locked={false} note="" coeff="" />;
    } else {
      return <InputGroup id={i} key={i} changed={actions} locked={el.locked} note={el.note} coeff={el.coeff} />;
    }
  });

  const average = displayAverage ? <Average notes={state.notes} reset={actions} /> : null;
  // State with [{id,note,coeff},...]
  // When length >= 10, keep the state from being tampered with and display average
  return (
    <>
      {formGroups}
      {average}
    </>
  );
};

export default Form;
