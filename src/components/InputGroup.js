import React from "react";
import "./InputGroup.scss";
import { useState, useEffect } from "react";

const InputGroup = (props) => {
  const { id, changed, locked } = props;
  const [note, setNote] = useState("");
  const [coeff, setCoeff] = useState("");

  useEffect(() => {
    setNote(props.note);
    return () => {};
  }, [props.note]);
  useEffect(() => {
    setCoeff(props.coeff);
    return () => {};
  }, [props.coeff]);

  return (
    <>
      <input
        required
        type="number"
        name="note"
        placeholder="Note"
        value={note}
        onChange={(e) => setNote(e.target.value)}
        disabled={locked}
      />
      &nbsp;
      <input
        type="number"
        name="coeff"
        placeholder="Coeff"
        value={coeff || 1}
        onChange={(e) => setCoeff(e.target.value)}
        disabled={locked}
      />
      &nbsp;
      <button disabled={!locked} onClick={(e) => changed({ id }, "EDIT")}>
        Edit
      </button>
      &nbsp;
      <button disabled={locked} onClick={(e) => changed({ id, note, coeff }, "ADD")}>
        ✔
      </button>
      &nbsp;
      <button disabled={!locked} onClick={(e) => changed({ id }, "DELETE")}>
        ✖
      </button>
      &nbsp;
      <br />
    </>
  );
};

export default InputGroup;
