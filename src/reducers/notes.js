export const reducer = (state, action) => {
  let newNotes = [...state.notes];
  switch (action.type) {
    case "ADD":
      newNotes[action.payload.id] = { ...action.payload, locked: true };
      return { ...state, notes: newNotes };
    case "EDIT":
      newNotes[action.payload.id].locked = false;
      return { ...state, notes: newNotes };
    case "DELETE":
      newNotes[action.payload.id].note = "";
      newNotes[action.payload.id].coeff = "";
      newNotes[action.payload.id].locked = false;
      return { ...state, notes: newNotes };
    case "RESET":
      newNotes.map((note) => {
        note.note = "";
        note.coeff = "";
        note.locked = false;
        return note;
      });
      return { ...state /*...*/ };
    default:
      return state;
  }
};
